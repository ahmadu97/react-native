# Getting Started
Lets get React Native up and running.

## Preliminaries
First you need to have node installed on your computer:

- [Node](https://nodejs.org/en/download/) (which comes with [npm](http://npmjs.com))

Check that Node is installed through terminal or command line 
```bash
# Check for Node
node -v
```

## Creating a new React Native Project 

Install creat-react-native-app command line utility:
```bash
npm install -g create-react-native-app
```

Run the following commands to create a new React Native project called "MyFirstReactNativeApp":
```bash
create-react-native-app MyFirstReactNativeApp

cd MyFirstReactNativeApp
npm start
```

If you get an error that server could not start run the provided snipets and rerun the command:
```bash
npm start
```

Install the Expo client app on your iOS or Android phone and connect to the same wireless network as your computer.
Using the Expo app, scan the QR code from your terminal or enter your phone number to get a url to open your project.

## Modifying Your App

Open App.js in your text editor of choice and edit some lines. 
The application should reload automatically once you save your changes.

## Congrats Your First React Native App Is Running!




import React, { Component } from 'react';
import { Alert, AppRegistry, Button, StyleSheet, View } from 'react-native';

export default class Horscope extends Component {
  _arries() {
    Alert.alert('You are Aries ♈️')
  }
  _taurus() {
    Alert.alert('You are Taurus ♉️')
  }
  _gemini() {
    Alert.alert('You are Gemini ♊️')
  }
  _cancer() {
    Alert.alert('You are Cancer ♋️')
  }
  _leo() {
    Alert.alert('You are Leo ♌️')
  }
  _virgo() {
    Alert.alert('You are Virgo ♍️')
  }
  _libra() {
    Alert.alert('You are Libra ♎️')
  }
  _scorpio() {
    Alert.alert('You are Scorpio ♏️')
  }
  _sagittarius() {
    Alert.alert('You are Sagittarius ♐️')
  }
  _capricorn() {
    Alert.alert('You are Capricorn ♑️')
  }
  _aquarius() {
    Alert.alert('You are Aquarius ♒️')
  }
  _pisces() {
    Alert.alert('You are Pisces ♓️')
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.buttonContainer}>
          <Button
            onPress={this._arries}
            title="Mar 21 - Apr 19"
          />
        </View>
        <View style={styles.buttonContainer}>
          <Button
            onPress={this._taurus}
            title="Apr 20 - May 20"
          />
        </View>
        <View style={styles.buttonContainer}>
          <Button
            onPress={this._gemini}
            title="May 21 - Jun 20"
          />
          </View>
          <View style={styles.buttonContainer}>
          <Button
            onPress={this._cancer}
            title="Jun 21 - Jul 22"
          />
           </View>
        <View style={styles.buttonContainer}>
          <Button
            onPress={this._leo}
            title="Jul 23 - Aug 22"
          />
           </View>
        <View style={styles.buttonContainer}>
          <Button
            onPress={this._virgo}
            title="Aug 23 - Sep 22"
          />
           </View>
        <View style={styles.buttonContainer}>
          <Button
            onPress={this._libra}
            title="Sep 23 - Oct 22"
          />
           </View>
        <View style={styles.buttonContainer}>
          <Button
            onPress={this._scorpio}
            title="Oct 23 - Nov 21"
          />
           </View>
        <View style={styles.buttonContainer}>
          <Button
            onPress={this._sagittarius}
            title="Nov 22 - Dec 21"
          />
           </View>
        <View style={styles.buttonContainer}>
          <Button
            onPress={this._capricorn}
            title="Dec 22 - Jan 19"
          />
           </View>
        <View style={styles.buttonContainer}>
          <Button
            onPress={this._aquarius}
            title="Jan 20 - Feb 18"
          />
           </View>
        <View style={styles.buttonContainer}>
          <Button
            onPress={this._pisces}
            title="Feb 19 - Mar 20"
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
   flex: 1,
   justifyContent: 'center',
  },
  buttonContainer: {
    margin: 1
  },
})

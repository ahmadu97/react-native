# Welcome to my repository which will help introduce you to React Native.

# Clone this repository
```bash
git clone https://ahmadu97@bitbucket.org/ahmadu97/react-native.git
```

Move the Node_Modules folder into React-Native-ZodiacSign folder

# Go into the repository
```bash
cd react-native
```

# Install Dependencies 
```bash
npm install
```

## References 

- [React Native Guide](https://facebook.github.io/react-native/)
- [Intro to Native Components](https://www.linkedin.com/learning/learning-react-native-2/introduction-to-native-components)
- [ES2015 Guide](https://babeljs.io/learn-es2015/)
